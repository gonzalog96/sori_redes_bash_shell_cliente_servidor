#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

int main(int argc,char **argv)
{
  char str[100];
  //////////////////////////////////////
  // Definir la direccion del server
  //////////////////////////////////////
  struct sockaddr_in my_server_addr;
  bzero(&my_server_addr,sizeof my_server_addr); 
  my_server_addr.sin_family=AF_INET;
  my_server_addr.sin_port=htons(22000);

  //Setear la direccion IP en my_server_addr a “127.0.0.1” (localhost) si el servidor esta en la misma maquina.
  // inet_pton - convierte direcciones IPv4/IPv6 de texto a forma binaria

  // :: 192.168.1.3 es la direccion IP del equipo que tiene a server.c >> modificar a gusto.
  inet_pton(AF_INET,"192.168.1.3",&(my_server_addr.sin_addr));


  //////////////////////////////////////
  // Definir el socket
  //////////////////////////////////////

  int socket_client_fd;
  socket_client_fd = socket(AF_INET, SOCK_STREAM, 0);

  //la funcion connect() conecta al dispositivo cuya direccion y puerto es my_server_addr. 
  connect(socket_client_fd, (struct sockaddr *) &my_server_addr,sizeof(my_server_addr));

  //Ahora el cliente puede enviar y recibir datos  
  char sendline[100];
  char recvline[100];

  //////////////////////////////////////////
  // Empieza el ciclo principal del cliente
  //////////////////////////////////////////

  while(1){
    //Primero limpiar los buffers sendline and recvline
    bzero( sendline, 100);
    bzero( recvline, 100);

    //leer un string de stdin (la terminal) y guardarlo en sendline 
    //el usuario tiene que escribir algo
    fgets(sendline, 100, stdin); /*stdin = 0 , for standard input */

    //escribir  sendline en  socket_client_fd - enviar mensaje.
    write(socket_client_fd, sendline, strlen(sendline)+1);
    
    // mensaje enviado ::
    printf("[client] Ud. envio el siguiente mensaje: %s", sendline);
        
    // leemos desde el servidor ::
    read(socket_client_fd,recvline,100);

    // imprimir en pantalla lo que lei ::
    printf("[server] El mensaje recibido fue: ");
    printf("%s",recvline);

    // ::
    // creamos un string de hasta 100 caracteres (utilizaremos 4 solamente)
    // llamado 'target': en el mismo, se almacenada la respuesta del servidor.
    // por defecto, si el cliente envia el mensaje 'salir', el servidor responde 'chau'
    // y tambien se desconecta ...
    // 'chau' es, entonces, el string que permite desconectar a un cliente.
    char target[100];
    strncpy(target, recvline, 4);
    target[4] = '\0'; // para finalizar el string.

    // verificacion de que el servidor envio el string "chau".
    if (strcmp(target, "chau") == 0)	
    {
	printf("la respuesta recibida fue 'chau' (desconexion total).\n");
	printf("adios amigos!\n");
        break;
    }
  }
  return 0; //EXIT_SUCCESS;   
}
